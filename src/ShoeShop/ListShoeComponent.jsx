import React, { Component } from "react";
import ItemShoeComponent from "./ItemShoeComponent";

export default class ListShoeComponent extends Component {
  render() {
    return (
      <div>
        <h4>ListShoe</h4>
        <div className="row">
          {this.props.data.map((item, index) => {
            return (
              <ItemShoeComponent
                key={index}
                item={item}
                handleAddToCart={this.props.handleAddToCart}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

import React, { Component } from "react";

export default class ItemShoeComponent extends Component {
  render() {
    let { image, name, price } = this.props.item;
    return (
      <div className="card col-3">
        <img src={image} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{price}$</p>
          <button
            onClick={() => {
              this.props.handleAddToCart(this.props.item);
            }}
            className="btn btn-primary"
          >
            Add
          </button>
        </div>
      </div>
    );
  }
}

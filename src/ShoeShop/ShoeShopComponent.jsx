import React, { Component } from "react";
import CartComponent from "./CartComponent";
import { data } from "./data";
import ListShoeComponent from "./ListShoeComponent";

export default class ShoeShopComponent extends Component {
  state = {
    data: data,
    cart: [],
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id === shoe.id;
    });
    if (index === -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong += 1;
    }
    this.setState({
      cart: cloneCart,
    });
  };
  handleDelete = (idShoe) => {
    let index = this.state.cart.findIndex((item) => {
      return item.id === idShoe;
    });
    if (index !== -1) {
      let newCart = [...this.state.cart];
      newCart.splice(index, 1);
      this.setState({
        cart: newCart,
      });
    }
    // Co the dung filter de lam nhanh hon thi khong can tim index va splice
  };
  handleChangeQuantity = (id, soLuong) => {
    let cloneCart = [...this.state.cart];

    let index = cloneCart.findIndex((item) => {
      return item.id === id;
    });
    cloneCart[index].soLuong += soLuong;
    this.setState({
      cart: cloneCart,
    });
  };
  render() {
    return (
      <div className="container">
        <h2>Shoe Shop</h2>
        <div className="row">
          <div className="col-6">
            <CartComponent
              cart={this.state.cart}
              handleDelete={this.handleDelete}
              handleChangeQuantity={this.handleChangeQuantity}
            />
          </div>
          <div className="col-6">
            <ListShoeComponent
              data={this.state.data}
              handleAddToCart={this.handleAddToCart}
            />
          </div>
        </div>
      </div>
    );
  }
}

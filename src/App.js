import "./App.css";
import ShoeShopComponent from "./ShoeShop/ShoeShopComponent";

function App() {
  return (
    <div className="App">
      <ShoeShopComponent />
    </div>
  );
}

export default App;
